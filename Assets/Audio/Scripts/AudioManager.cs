﻿using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [Header("Audio Mixer")]
    [SerializeField] AudioMixer audioMixer;

    [Header("Audio Sources")]
    [SerializeField] AudioSource audSourceSFXGameplay;
    [SerializeField] AudioSource audSourceBGM;

    [Header("Audio Clip Sets")]
    public SO_AudioClipSetGameplay audioClipSetGameplay;

    [Header("Parameters")]
    [SerializeField] float musicFadeInSecs = 2.0f;

    // Audio clips to unload when changing scenes
    private HashSet<AudioClip> audioClipsToUnload = new HashSet<AudioClip>();

    // Store the currently used BGM audio source
    private AudioSource audSourceBGMActive;

    private string bgmVolumeParamName = "BGMVolume";

    #region Monobehaviour

    private void Awake()
    {
        // Singleton
        if (instance == null)
        {
            Debug.Log("singleton!");
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Debug.Log("Destroy");
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SceneManager.activeSceneChanged += ChangedActiveScene;
    }

    #endregion

    #region Utility

    public enum AudioCategory {
        SFXPriority,
        SFXUI,
        SFXGameplay,
        BGM
    }

    private AudioSource GetAudioSource(AudioCategory category)
    {
        switch (category)
        {
            case AudioCategory.SFXGameplay:
                return audSourceSFXGameplay;
            case AudioCategory.BGM:
                Debug.LogError("Use PlayBGM function instead to play BGM");
                return null;
            default:
                return audSourceSFXGameplay;
        }
    }

    private static IEnumerator StartFade(AudioMixer audioMixer, string exposedParam, float duration, float targetVolume, bool pauseAfterFade = false, AudioSource audSource = null)
    {
        float currentTime = 0;
        float currentVol;
        audioMixer.GetFloat(exposedParam, out currentVol);
        currentVol = Mathf.Pow(10, currentVol / 20);
        float targetValue = Mathf.Clamp(targetVolume, 0.0001f, 1);

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            float newVol = Mathf.Lerp(currentVol, targetValue, currentTime / duration);
            audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
            yield return null;
        }

        yield break;
    }


    #endregion

    #region Play BGM

    public void PlayBGM(AudioClip audioClip)
    {
        // Play the new BGM and fade in
        audSourceBGM.clip = audioClip;
        audioMixer.SetFloat(bgmVolumeParamName, 0f);
        audSourceBGM.Play();
        audSourceBGM.loop = true;
    }

    public void PlayBGM(SO_AudioClipSetGameplay.ClipEnum e)
    {
        PlayBGM(audioClipSetGameplay.GetAudioClip(e));
    }

    public void FadeOutBGM()
    {
        StartCoroutine(StartFade(audioMixer, bgmVolumeParamName, musicFadeInSecs, 0f, true, audSourceBGM));
    }

    #endregion

    #region PlaySoundLooping

    public AudioSource PlaySoundLooping(AudioClip audioClip, AudioCategory category = AudioCategory.SFXGameplay)
    {
        AudioSource audioSource = GetAudioSource(category);
        if (category != AudioCategory.SFXUI && category != AudioCategory.BGM)
            audioClipsToUnload.Add(audioClip);
        audioSource.clip = audioClip;
        audioSource.Play();
        audioSource.loop = true;
        return audioSource;
    }

    public void PlaySoundLooping(SO_AudioClipSetGameplay.ClipEnum e, AudioCategory category = AudioCategory.SFXGameplay)
    {
        PlaySoundLooping(audioClipSetGameplay.GetAudioClip(e), category);
    }

    #endregion

    #region PlaySoundOneShot

    public AudioSource PlaySoundOneShot(AudioClip audioClip, AudioCategory category = AudioCategory.SFXGameplay)
    {
        AudioSource audio = GetAudioSource(category);
        if (category != AudioCategory.SFXUI && category != AudioCategory.BGM)
            audioClipsToUnload.Add(audioClip);
        audio.PlayOneShot(audioClip);
        return audio;
    }

    public void PlaySoundOneShot(SO_AudioClipSetGameplay.ClipEnum e, AudioCategory category = AudioCategory.SFXGameplay)
    {
        PlaySoundOneShot(audioClipSetGameplay.GetAudioClip(e), category);
    }

    #endregion

    #region PlaySoundNormal

    public AudioSource PlaySoundNormal(AudioClip audioClip, AudioCategory category = AudioCategory.SFXGameplay)
    {
        AudioSource audio = GetAudioSource(category);
        if (category != AudioCategory.SFXUI && category != AudioCategory.BGM)
            audioClipsToUnload.Add(audioClip);
        audio.clip = audioClip;
        audio.Play();

        return audio;
    }

    public void PlaySoundNormal(SO_AudioClipSetGameplay.ClipEnum e, AudioCategory category = AudioCategory.SFXGameplay)
    {
        PlaySoundNormal(audioClipSetGameplay.GetAudioClip(e), category);
    }

    #endregion

    #region Stop/Pause 

    public AudioSource StopEffectSound(AudioCategory category = AudioCategory.SFXGameplay)
    {
        AudioSource audio = GetAudioSource(category);
        audio.Stop();
        return audio;
    }

    public AudioSource SetVolume(AudioCategory category, float volume)
    {
        AudioSource audio = GetAudioSource(category);
        audio.volume = volume;
        return audio;
    }

    public void PauseAll()
    {
        AudioListener.pause = true;
    }

    public void UnPauseAll()
    {
        AudioListener.pause = false;
    }

    #endregion

    #region Unload Audio

    private void ChangedActiveScene(Scene current, Scene next)
    {
        // Copy the hashset of audioclips to remove so that we don't cause an error when removing them in a loop
        HashSet<AudioClip> audioClipsCopy = new HashSet<AudioClip>(audioClipsToUnload);

        // Unload previously loaded audio clips
        foreach (var clip in audioClipsCopy)
            clip.UnloadAudioData();

        // Remove previously loaded clips from the hashset
        foreach (var clip in audioClipsCopy)
            audioClipsToUnload.Remove(clip);
    }

    #endregion
    
}