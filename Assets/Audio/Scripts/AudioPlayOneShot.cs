﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayOneShot : MonoBehaviour
{
    [SerializeField] AudioClip audioClip;
    [SerializeField] AudioManager.AudioCategory category;

    public void PlayOneShot()
    {
        AudioManager.instance.PlaySoundOneShot(audioClip, category);
    }
}
