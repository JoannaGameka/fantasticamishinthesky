﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "New Audio Clip Set", menuName = "Audio/Audio Clip Set (Gameplay)")]
public class SO_AudioClipSetGameplay : SO_AudioClipSet
{
    [SerializeField] List<SO_AudioClipGameplay> audioClips;

    public enum ClipEnum
    {
        BGMgameplay,
        repairing,
        repairComplete,
        buttonClick,
        crash,
        axe,
        buttonHover
    }

    public AudioClip GetAudioClip(ClipEnum e)
    {
        SO_AudioClipGameplay soAudioClip = audioClips.FirstOrDefault(ac => ac.clipEnum == e);

        if (soAudioClip == null)
        {
            Debug.LogError("Audio clip " + e + " missing in " + GetType());
            return null;
        }

        return soAudioClip.audioClip;
    }
}
