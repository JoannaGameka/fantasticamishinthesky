﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioUIButton : MonoBehaviour
{
    public void OnClickAudio()
    {
        AudioManager.instance.PlaySoundOneShot(SO_AudioClipSetGameplay.ClipEnum.buttonClick);
    }
}
