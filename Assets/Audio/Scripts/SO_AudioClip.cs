﻿using UnityEngine;

public class SO_AudioClip : ScriptableObject
{
    public AudioClip audioClip;
}
