﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFadeOutBGMOnStart : MonoBehaviour
{
    void Start()
    {
        AudioManager.instance.FadeOutBGM();
        Destroy(gameObject);
    }

}
