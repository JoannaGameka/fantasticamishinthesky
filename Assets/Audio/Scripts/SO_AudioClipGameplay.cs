﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Audio Clip", menuName = "Audio/Audio Clip (Gameplay)")]
public class SO_AudioClipGameplay : SO_AudioClip
{
    public SO_AudioClipSetGameplay.ClipEnum clipEnum;
}
