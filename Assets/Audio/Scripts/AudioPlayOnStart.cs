﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayOnStart : MonoBehaviour
{
    [SerializeField]
    SO_AudioClipSetGameplay.ClipEnum clipEnum;

    private void Start()
    {
        AudioManager.instance.PlayBGM(clipEnum);
        Destroy(gameObject);
    }
}
