﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayOneShotRandPool : MonoBehaviour
{
    [SerializeField] List<AudioClip> audioClips;
    [SerializeField] AudioManager.AudioCategory category;

    public void PlayOneShotRandPool()
    {
        float rand = Random.Range(0, audioClips.Count);
        int indexToPlay = (int)rand;
        AudioManager.instance.PlaySoundOneShot(audioClips[indexToPlay], category);
    }
}
