﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
    public TextMeshProUGUI highScoreText;

    public AudioSource soundAudioSource;
    public AudioClip buttonClickClip;

    private void Start()
    {
        ScoreManager.ResetScore();
        ScoreManager.LoadHighScore();

        if (highScoreText != null)
        {
            // Display high score
            highScoreText.text = string.Format(highScoreText.text, ScoreManager.highScore.ToString("0"));
        }
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void EasterEgg()
    {

    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void PlayButtonClickSFX()
    {
        if (soundAudioSource == null || buttonClickClip == null) return;
        soundAudioSource.PlayOneShot(buttonClickClip);
    }
}
