﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ResultManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;

    public AudioSource soundAudioSource;
    public AudioClip buttonClickClip;
    
    private void Start()
    {
        scoreText.text = string.Format(scoreText.text, ScoreManager.score.ToString("0"));
        ScoreManager.SaveHighScore();
        AudioManager.instance.PlaySoundOneShot(SO_AudioClipSetGameplay.ClipEnum.crash);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void PlayButtonClickSFX()
    {
        if (soundAudioSource == null || buttonClickClip == null) return;
        soundAudioSource.PlayOneShot(buttonClickClip);
    }
}
