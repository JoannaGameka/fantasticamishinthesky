﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GroundObjectSpawnControl : MonoBehaviour
{
    public SpawnData[] objectSpawnData;
    public float spawnFrequencyStart = 2f;
    public float spawnFrequencyMin = 5f;
    public float spawnFrequencyMax = 10f;
    public float objectMovementRate = 2f;
    public float despawnXPos = 10f;

    private float totalSpawnWeight = 0f;
    private float spawnTimer = 1f;
    private List<GameObject> objSpawned = new List<GameObject>();

    private void Awake()
    {
        foreach (SpawnData data in objectSpawnData)
        {
            data.obj.SetActive(false);
            totalSpawnWeight += data.chanceWeight;
        }
    }

    private void Start()
    {
        spawnTimer = spawnFrequencyStart;
    }

    private void Update()
    {
        // Object spawning
        spawnTimer -= Time.deltaTime;
        if (spawnTimer < 0f)
        {
            spawnTimer = Random.Range(spawnFrequencyMin, spawnFrequencyMax);

            float chance = Random.Range(0f, totalSpawnWeight);
            float accChance = 0f;
            GameObject newObj = null;
            foreach (SpawnData data in objectSpawnData)
            {
                // Spawn object
                if (chance < accChance + data.chanceWeight)
                {
                    newObj = Instantiate(data.obj);
                    newObj.SetActive(true);
                    objSpawned.Add(newObj);
                    Vector3 spawnPos = new Vector3();
                    spawnPos.x = Random.Range(data.spawnPosWindow.xMin, data.spawnPosWindow.xMax);
                    spawnPos.y = Random.Range(data.spawnPosWindow.yMin, data.spawnPosWindow.yMax);
                    newObj.transform.position = spawnPos;
                    break;
                }
                // Move to next one
                accChance += chance;
            }
        }

        // Move any active objects.
        foreach (GameObject x in objSpawned)
        {
            if (x == null) continue;

            x.transform.position += Vector3.right * objectMovementRate * Time.deltaTime;
            // Moved too far to the right? Destroy it.
            if (x.transform.position.x > despawnXPos)
            {
                Destroy(x);
            }
        }

        // Remove objects from a list if they do not exist.
        for (int i = 0; i < objSpawned.Count; i++)
        {
            if (objSpawned[i] == null)
            {
                objSpawned.RemoveAt(i);
                break;
            }
        }
    }

    [Serializable] public class SpawnData
    {
        public GameObject obj;
        public float chanceWeight;
        public Rect spawnPosWindow;
    }
}
