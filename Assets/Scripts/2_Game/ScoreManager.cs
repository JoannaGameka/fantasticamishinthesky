﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static float score;
    public static float highScore;

    public float scoreIncreaseRateOverTime = 100f;
    public TextMeshProUGUI scoreDisplay;

    public static void ResetScore()
    {
        score = 0f;
    }

    public static void AddScore(float points)
    {
        score += points;
        CheckHighscore();
    }

    private static void CheckHighscore()
    {
        if (score > highScore) highScore = score;
    }

    public static void SaveHighScore()
    {
        PlayerPrefs.SetFloat("highscore", highScore);
        PlayerPrefs.Save();
    }

    public static void LoadHighScore()
    {
        highScore = PlayerPrefs.GetFloat("highscore", 0f);
    }

    private void Update()
    {
        score += Time.deltaTime * scoreIncreaseRateOverTime;
        CheckHighscore();

        if (scoreDisplay != null)
        {
            scoreDisplay.text = score.ToString("0");
        }
    }
}
