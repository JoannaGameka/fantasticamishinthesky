﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineerPlayerControl : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 5f;

    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField]
    private AirplaneManager airplaneManager;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    private Vector2 offsetPosition;
    private List<Collider2D> holesCollidingWith = new List<Collider2D>();
    private Collider2D refuelArea;

    private void Start()
    {
        offsetPosition = transform.position - transform.parent.position;
    }

    private void Update()
    {
        // Movement
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float moveRate = movementSpeed * Time.deltaTime;

        if (horizontalInput > 0.0001f)
        {
            transform.Translate(Vector3.right * moveRate);
            spriteRenderer.flipX = false;
        }
        else if (horizontalInput < -0.0001f)
        {
            transform.Translate(Vector3.left * moveRate);
            spriteRenderer.flipX = true;
        }

        // Repair/Refuel
        if (Input.GetButtonDown("Action"))
        {
            if (refuelArea != null)
                airplaneManager.AddFuel();

            else if (holesCollidingWith.Count > 0)
            {
                foreach (var hole in holesCollidingWith)
                {
                    // This is inefficient I'M SORREHHH
                    AirplaneHole airplaneHole = hole.GetComponent<AirplaneHole>();
                    if (airplaneHole.isFixed)
                        continue;
                    else
                        airplaneHole.FixHole();
                }
                
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.ToLower() == "hole")
            holesCollidingWith.Add(collider);

        if (collider.tag.ToLower() == "refuel")
            refuelArea = collider;
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag.ToLower() == "hole")
            holesCollidingWith.Remove(collider);

        if (collider.tag.ToLower() == "refuel")
            refuelArea = null;
    }
}
