﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundQuadScroll : MonoBehaviour
{
    public float movementRate = 100f;

    public float teleportObjXPosThreshold = 20f;
    public float teleportObjXPosMovement = 40f;

    private void Update()
    {
        transform.Translate(Vector3.right * movementRate * Time.deltaTime);

        if (transform.position.x > teleportObjXPosThreshold)
        {
            transform.Translate(Vector3.left * teleportObjXPosMovement);
        }
    }
}
