﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LumberjackPlayerControl : MonoBehaviour
{
    public static LumberjackPlayerControl instance;
    public AirplaneManager airplaneManager;

    public Rigidbody2D rb;
    public float movementSpeed = 1f;
    public TextMeshPro text_wood;

    public AudioPlayOneShotRandPool depositWoodAudio;
    public AudioPlayOneShotRandPool shockAudio;

    private Animator mAnimator;
    private int woodCollected = 0;
    private float invincibility = 3f;

    private void Awake()
    {
        instance = this;
        mAnimator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        text_wood.text = "0";
    }

    private void Update()
    {
        invincibility -= Time.deltaTime;

        Vector2 currectPos2D = new Vector2(transform.position.x, transform.position.y);
        float vertInput = Input.GetAxisRaw("Vertical");
        float moveRate = movementSpeed * Time.deltaTime;

        if (vertInput > 0.0001f)
        {
            //rb.MovePosition(currectPos2D + (Vector2.up * moveRate));
            transform.Translate(Vector3.up * moveRate);
        }
        else if (vertInput < -0.0001f)
        {
            //rb.MovePosition(currectPos2D + (Vector2.down * moveRate));
            transform.Translate(Vector3.down * moveRate);
        }
    }

    private void CollectWood()
    {
        woodCollected += 1;
        text_wood.text = woodCollected.ToString();
        AudioManager.instance.PlaySoundOneShot(SO_AudioClipSetGameplay.ClipEnum.axe);
    }

    private void DepositWood()
    {
        if (woodCollected <= 0) return;
            
        airplaneManager.AddWood(woodCollected);
        woodCollected = 0;
        text_wood.text = woodCollected.ToString();
        depositWoodAudio.PlayOneShotRandPool();
    }

    private void GetShocked()
    {
        if (invincibility > 0) return;
        
        invincibility = 2f;
        woodCollected = 0;
        text_wood.text = woodCollected.ToString();
        shockAudio.PlayOneShotRandPool();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string colTag = collision.tag.ToLower();
        switch (colTag)
        {
            case "plane":
                DepositWood();
                break;
            case "wood":
                CollectWood();
                {
                    Animator aPole = collision.GetComponentInParent<Animator>();
                    if (aPole != null)
                    {
                        aPole.SetBool("chop", true);
                    }
                    if (mAnimator != null)
                    {
                        mAnimator.Play("Chop");
                    }
                }
                Destroy(collision.gameObject);
                break;
            case "shock":
                GetShocked();
                {
                    Animator aPole = collision.GetComponentInParent<Animator>();
                    if (aPole != null)
                    {
                        aPole.SetBool("chop", true);
                    }
                    if (mAnimator != null)
                    {
                        mAnimator.Play("Shock");
                    }
                }
                break;
            default:
                Debug.Log("Touched something tagged as " + colTag);
                break;
        }
    }
}
