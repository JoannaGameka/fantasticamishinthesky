﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirplaneHole : MonoBehaviour
{
    public bool isFixed { get; private set; }
    public Sprite hole;
    public Sprite patch;
    private SpriteRenderer spriteRenderer;
    private Action<AirplaneHole> callbackFix;

    private void Start()
    {
        AirplaneManager airplaneManager = GetComponentInParent<AirplaneManager>();
        callbackFix = airplaneManager.FixHole;
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    public void OnMouseDown()
    {
        FixHole();
    }

    public void FixHole()
    {
        callbackFix.Invoke(this);
    }

    public void UpdateFixedHole()
    {
        isFixed = true;
        
        spriteRenderer.color = Color.white;
        spriteRenderer.sprite = patch;
    }

    public void DestroyHole()
    {
        isFixed = false;
        
        spriteRenderer.color = Color.white;
        spriteRenderer.sprite = hole;
    }
}
