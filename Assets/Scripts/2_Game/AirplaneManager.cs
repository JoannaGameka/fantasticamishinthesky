﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class AirplaneManager : MonoBehaviour
{
    #region Exposed Properties
    [Header("Initialisation")]

    [SerializeField]
    private float initialRateOfClimb = 0f;

    [SerializeField]
    [Tooltip("Time to wait before holes start spawning.")]
    private float readyTime = 2f;

    [SerializeField]
    [Tooltip("Holes spawn every x seconds.")]
    private float holeSpawnRateEveryXSecs = 2f;

    [SerializeField]
    [Tooltip("Decline rate = this value x nr of holes")]
    private float holeDeclineRate = 1f;

    [SerializeField]
    [Tooltip("How much wood it takes to fix a hole.")]
    private int holeRepairCostInWood = 1;

    [SerializeField]
    [Tooltip("How much lift fuel provides.")]
    private float fuelClimbRate = 2f;

    [SerializeField]
    [Tooltip("How long fuel lasts.")]
    private float fuelDuration = 0.5f;

    [SerializeField]
    [Tooltip("How much wood it takes to add fuel.")]
    private int fuelCostInWood = 1;

    [Header("Debugging")]
    [SerializeField]
    private float rateOfClimb;

    [SerializeField]
    private int amountOfWood = 0;

    [SerializeField]
    private Animator airplaneSpriteAnimator;

    [Header("For programmer use")]
    [SerializeField]
    private List<AirplaneHole> airplaneHoles;
    [SerializeField]
    private TMP_Text textFuelDuration;
    [SerializeField]
    private TMP_Text textWoodAmount;
    #endregion

    #region Variables

    private float fuelLevel;
    private float elevation;
    private float distance;
    private float leftBorder;
    private float rightBorder;
    private float bottomBorder;
    private float topBorder;
    private Vector3 playerSize;
    private float lastTimeSpawnedHole;
    private float fuelLastsTillTime;

    private List<AirplaneHole> destroyedHoles = new List<AirplaneHole>();

    #endregion

    #region Monobehaviours

    private void Start()
    {
        // Initialisation
        rateOfClimb = initialRateOfClimb;
        StartCoroutine(IEInitHoleCreation());

        // Calculate boundaries of the screen
        playerSize = this.GetComponent<SpriteRenderer>().bounds.size;
        distance = (transform.position - Camera.main.transform.position).z;
        leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).x + (playerSize.x / 2);
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance)).x - (playerSize.x / 2);
        bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).y + (playerSize.y / 2);
        topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance)).y - (playerSize.y / 2);
    }

    private void Update()
    {
        // Update rate of climb of the airplane
        // If there is fule, airplane always climbs
        bool hasFuel = fuelLastsTillTime > Time.time;
        airplaneSpriteAnimator.SetBool("animate", hasFuel);
        if (hasFuel)
        {
            rateOfClimb = fuelClimbRate;
        }
        else
        {
            rateOfClimb = (holeDeclineRate > 0 ? -holeDeclineRate : holeDeclineRate) * destroyedHoles.Count;
        }

        // Move the plane up/down based on the rateOfClimb
        transform.Translate(Vector3.up * Time.deltaTime * rateOfClimb, Space.World);

        // Clamp to the boundaries of the scene
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
            Mathf.Clamp(transform.position.y, bottomBorder, topBorder),
            transform.position.z);

        // For debugging??
        if (textFuelDuration != null)
            textFuelDuration.text = (Mathf.Clamp(fuelLastsTillTime-Time.time, 0, fuelDuration)).ToString();

        if (textWoodAmount != null)
            textWoodAmount.text = amountOfWood.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Game over
        if (collision.tag.ToLower() == "shock")
        {
            SceneManager.LoadScene("3_Result");
        }
    }

    #endregion

    #region Fuel

    public void AddFuel()
    {
        // Not enough wood to refule
        if (amountOfWood < fuelCostInWood)
            return;

        fuelLastsTillTime = Time.time + fuelDuration;
        amountOfWood -= fuelCostInWood;
    }

    #endregion

    #region Holes

    // Do not call this method, use AirplaneHole.FixHole() instead
    public void FixHole(AirplaneHole hole)
    {
        // hole already fixed or not enough wood to repair
        if (hole.isFixed || amountOfWood < holeRepairCostInWood)
            return;

        AudioManager.instance.PlaySoundOneShot(SO_AudioClipSetGameplay.ClipEnum.repairComplete);
        amountOfWood -= holeRepairCostInWood;
        hole.UpdateFixedHole();
        airplaneHoles.Add(hole);
        destroyedHoles.Remove(hole);
    }

    private void DestroyRandomHole()
    {
        // If there are no holes to destroy, do nothing
        if (airplaneHoles.Count == 0)
            return;

        // Choose a random hole to destroy
        int randomIndex = Random.Range(0, airplaneHoles.Count);
        AirplaneHole holeToDestroy = airplaneHoles[randomIndex];

        // Move the airplaneHole to the destroyed holes list
        destroyedHoles.Add(holeToDestroy);
        airplaneHoles.Remove(holeToDestroy);

        // DESTROYY
        holeToDestroy.DestroyHole();
    }

    IEnumerator IEInitHoleCreation()
    {
        yield return new WaitForSeconds(readyTime);
        StartCoroutine(IESpawnHole());
    }

    IEnumerator IESpawnHole()
    {        
        DestroyRandomHole();
        yield return new WaitForSeconds(holeSpawnRateEveryXSecs);
        StartCoroutine(IESpawnHole());
    }

    #endregion

    #region Wood

    public void AddWood(int amount)
    {
        amountOfWood += amount;
    }

    #endregion
}
